#include <iostream>
#include <string>
#include "sqlite3.h"

using std::cout;
using std::endl;
using std::string;

void checkRC(sqlite3 * db, int rc, char * zErrMsg);

int main()
{
	int rc = 0;
	sqlite3 * db = nullptr;
	char * zErrMsg = nullptr;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc != SQLITE_OK)
	{
		cout << "SQL Open Database Error: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
		return 1;
	}

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name text)", nullptr, nullptr, &zErrMsg);
	checkRC(db, rc, zErrMsg);

	rc = sqlite3_exec(db, "insert into people(name) values(\"ron\")", nullptr, nullptr, &zErrMsg);
	checkRC(db, rc, zErrMsg);

	rc = sqlite3_exec(db, "insert into people(name) values(\"modi\")", nullptr, nullptr, &zErrMsg);
	checkRC(db, rc, zErrMsg);

	rc = sqlite3_exec(db, "insert into people(name) values(\"dani\")", nullptr, nullptr, &zErrMsg);
	checkRC(db, rc, zErrMsg);

	rc = sqlite3_exec(db, "update people set Name = \"almogigi\" where id = last_insert_rowid()", nullptr, nullptr, &zErrMsg);
	checkRC(db, rc, zErrMsg);

	sqlite3_close(db);
	system("pause");
	return 0;
}

void checkRC(sqlite3 * db, int rc, char * zErrMsg)
{
	if (rc != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_close(db);
		system("pause");
		exit(1);
	}
}